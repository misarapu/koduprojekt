/**
 * Hypikaknad
 * Loodud 10. detsember 2015
 *
 * @author Mikk Sarapuu <mikksarapuu@gmail.com>
 */
package main;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Selles klassis on kirjutatud Raamatukogu juurde kuuluvad hypikaknad, mis avanevad
 * vastavade akstioonide korral. On defineeritud yks yldine boolean-tyypi muutuja
 * "vastus".
 */
public class Hypikaknad {

    static boolean vastus;

    /**
     * Klassis on defineeritud hypikakna moodustavad atribuudid. Hypikakent kasutatkse
     * juhul, kui kasutaja on andmete lahtrisse sisestamisel eksinud kriteeriumite
     * vastu. Kui hypikaken on avanenud, ei saa peaakent kasutada - hypikaken tuleb enne
     * sulgeda.
     *
     * @param s6num on hypikaknal esinev tekst
     */
    public static void sisestamiseViga(String s6num) {

        Stage aken = new Stage();
        aken.initModality(Modality.APPLICATION_MODAL);
        aken.setTitle("Sisestuse viga");
        aken.setMinWidth(500);
        aken.setMinHeight(130);
        aken.setResizable(false);
        Label m2rkus = new Label();
        m2rkus.setText(s6num);
        Button sulgemisNupp = new Button("OK");
        sulgemisNupp.setMinSize(70, 10);
        sulgemisNupp.setOnAction(e -> {
            aken.close();
        });
        VBox asetus = new VBox(10);
        asetus.getChildren().addAll(m2rkus, sulgemisNupp);
        asetus.setAlignment(Pos.CENTER);
        Scene vaade = new Scene(asetus);
        aken.setScene(vaade);
        aken.showAndWait();
    }

    /**
     * Klassis on defineeritud hypikakna moodustavad atribuudid. Hypikaknas kysitakse
     * kasutajalt, kas ta on vastava aktsiooni tegemises kindel. Kui hypikaken on avanenud,
     * ei saa peaakent kasutada - hypikaken tuleb enne
     * sulgeda.
     *
     * @param s6num hypikaknal esinev tekst
     * @return boolean-tyypi v22rtus
     */
    public static boolean aknaSulgemine(String s6num) {
        Stage aken = new Stage();
        aken.initModality(Modality.APPLICATION_MODAL);
        aken.setTitle("Sulgemine");
        aken.setMinWidth(500);
        aken.setMinHeight(130);
        aken.setResizable(false);
        Label m2rkus = new Label();
        m2rkus.setText(s6num);
        Button jahNupp = new Button("Jah");
        jahNupp.setMinSize(70, 10);
        Button eiNupp = new Button("Ei");
        eiNupp.setMinSize(70, 10);
        Button salNupp = new Button("Salvesta");
        jahNupp.setOnAction(e2 -> {
            vastus = true;
            aken.close();
        });
        eiNupp.setOnAction(e3 -> {
            vastus = false;
            aken.close();
        });
        HBox nupudHb = new HBox(5);
        nupudHb.getChildren().addAll(m2rkus, jahNupp, eiNupp);
        nupudHb.setAlignment(Pos.CENTER);
        VBox asetus = new VBox(10);
        asetus.getChildren().addAll(m2rkus, nupudHb);
        asetus.setAlignment(Pos.CENTER);
        Scene vaade = new Scene(asetus);
        aken.setScene(vaade);
        aken.showAndWait();
        return vastus;
    }
}