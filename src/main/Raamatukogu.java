/**
 * Raamatukogu
 * Loodud 10. detsember 2015
 *
 * @author Mikk Sarapuu <mikksarapuu@gmail.com>
 */
package main;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.control.cell.PropertyValueFactory;

import javafx.geometry.Insets;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.List;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import java.util.ArrayList;
import java.util.Calendar;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Selles klassis luuakse aken, mis sisaldab tabelit objekti Raamatu parameetridega.
 * Lisaks tabelile asuvad aknas erinevad nupud ja lahtrid, mis v6imaldavad tabelit
 * vajaduse korral t2iendada. K6ik andmed loetakse ja salvestatakse faili "arhiiv.csv",
 * mis asub projecki "src" kaustas. Samuti on aknasse paigutad vaade nimekirjast, kus on
 * n2htaval failis olevad read. Nimekirjavaate puhul on faili ridadelt eemaldatud
 * Raamatu parameetreid kirjeldavate tekstide vahelised symbolid, mis aitavad failist
 * paremeetrite kirjeid eraldada. Raamatukogu klass laieneb Application klassi.
 * <p>
 * Samuti on selles klassis kirjutad erinevad meetodid ja teise klassid.
 */
public class Raamatukogu extends Application {

    /**
     * Klassi alguses on v2lja toodud yldised muutujad, mis leiavad kasutust mitmes
     * teises klassis ja meetodis. M6ned muutuja yldmuutujaks defineerimine v6ib
     * osutuda oluliseks alles projekti edasi arendamisel ning hetkel ei ole vajadus
     * neid defineerida kui yldmuutujaid, kuid autor on otsustanud vastavad muutujad
     * j2tta yldmuutujateks.
     */
    private TableView<Raamat> tabel;
    private ObservableList<Raamat> andmed;
    private TextField pealkiriSisend, autorSisend, aastaSisend, kogusSisend, otsingSisend;
    private ListView otsingList = new ListView();
    private Label raamatudErinevad, raamatudKokku;
    private TableColumn<Raamat, String> pealkiriTulp, autorTulp;
    private TableColumn<Raamat, Integer> aastaTulp, kogusTulp;

    /**
     * Meetod, mis k2ivtab rakenduse.
     *
     * @param args argumendid
     */
    public static void main(String[] args) {

        Application.launch(args);
    }

    /**
     * Meetodiga luukse peamiselt Raamatukogu kasutajaliides. Esmalt defineeritakse
     * programmi akna omadused. Teiseks konstrueeritakse tabel, mille tulpades esitatakse
     * Raamatu parameetrid. Kolmandaks defineeritkase k6iksugused teksid ja pildid, mis
     * akna taustal kuvatakse. Neljandaks defineeritakse Raamatute nimekirja vaade, milles
     * kuvatakse "arhiiv.csv" sisu. Viiendaks defineeritakse k6ik kasutajale m6eldud
     * tekstisisestuse lahtrid. Kuuendakse on defineeritud kasutajele m6eldud nupud, mille
     * abil manipuleeritakse teiste atribuutidega. Seitsmendaks m22ratakse akna atribuutide
     * paigutus.
     *
     * @param peaAken on aken, kus kuvatakse Raamatukogu k6ik atribuudid
     * @throws Exception v2ljastatkse, kui tekib probleeme failiga talitledes
     */
    @SuppressWarnings({"unchecked"})
    @Override
    public void start(Stage peaAken) throws Exception {

        // Peaakna omadused
        peaAken.setTitle("Koduraamatukogu");
        peaAken.setResizable(false);
        peaAken.setOnCloseRequest(e -> {
            e.consume();
            if (Hypikaknad.aknaSulgemine("Programmi sulgemisel kaovad kõik salvestamata kirjed.\n" +
                    "Kas oled kindel, et soovid väljuda?") == true) {
                peaAken.close();
            }
        });

        // Tabeli omadused ja ylesehitus
        tabel = new TableView<>();
        andmed = tabeliT2itmine(); // kui csv fail puudub, siis siin see luuakse
        tabel.setItems(andmed);
        andmed.addListener(new ListChangeListener<Raamat>() {
            @Override
            public void onChanged(Change<? extends Raamat> c) {
                int kokku = 0;
                for (Raamat kogus : tabel.getItems()) {
                    kokku = kokku + kogusTulp.getCellObservableValue(kogus).getValue();
                }
                raamatudErinevad.setText("Erinevaid raamatuid : " + andmed.size());
                raamatudKokku.setText("Raamatuid kokku: " + kokku);
            }
        });
        pealkiriTulp = new TableColumn<>("Pealkiri");
        pealkiriTulp.setMinWidth(250);
        pealkiriTulp.setCellValueFactory(new PropertyValueFactory<>("pealkiri"));
        autorTulp = new TableColumn<>("Autor");
        autorTulp.setMinWidth(120);
        autorTulp.setCellValueFactory(new PropertyValueFactory<>("autor"));
        aastaTulp = new TableColumn<>("Aasta");
        aastaTulp.setMinWidth(25);
        aastaTulp.setStyle("-fx-alignment: baseline-center");
        aastaTulp.setCellValueFactory(new PropertyValueFactory<>("aasta"));
        kogusTulp = new TableColumn<>("Kogus");
        kogusTulp.setMinWidth(20);
        kogusTulp.setStyle("-fx-alignment: baseline-center");
        kogusTulp.setCellValueFactory(new PropertyValueFactory<>("kogus"));
        tabel.getColumns().setAll(pealkiriTulp, autorTulp, aastaTulp, kogusTulp);
        tabel.setPrefHeight(400);
        tabel.setPrefWidth(580);
        tabel.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tabel.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        // Tekstid peaaknal
        Image logo = new Image(getClass().getResourceAsStream("/logo.png"));
        Label tiitel = new Label("Koduraamatukogu");
        tiitel.setFont(Font.font("Times New Roman", 50));
        tiitel.setGraphic(new ImageView(logo));
        Label otsing = new Label("Otsing");
        otsing.setFont(Font.font("Times New Roman", 15));
        Label stats = new Label("Statistika:");
        raamatudErinevad = new Label("Erinevaid raamatuid : " + andmed.size());
        raamatudKokku = new Label("Raamatuid kokku: " + listiNumbriteSumma(3));
        stats.setFont(Font.font("Times New Roman", 20));
        raamatudErinevad.setFont(Font.font("Times New Roman", 20));
        raamatudKokku.setFont(Font.font("Times New Roman", 20));

        // Otsingu tulemuste aken
        otsingList.setMaxSize(500, 100);
        ObservableList<String> sisestused = tooresNimekiri();
        otsingList.setItems(sisestused);

        // Andmete sisestamise lahtrid
        pealkiriSisend = new TextField();
        pealkiriSisend.setMinSize(310, 10);
        pealkiriSisend.setPromptText("Pealkiri");
        autorSisend = new TextField();
        autorSisend.setMinSize(100, 10);
        autorSisend.setPromptText("Autor");
        aastaSisend = new TextField();
        aastaSisend.setMaxSize(50, 10);
        aastaSisend.setPromptText("Aasta");
        kogusSisend = new TextField();
        kogusSisend.setMaxSize(50, 10);
        kogusSisend.setPromptText("Kogus");
        otsingSisend = new TextField();
        otsingSisend.setMaxSize(500, 10);
        otsingSisend.setPromptText("Otsi...");
        otsingSisend.textProperty().addListener(
                new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> observable, String vana, String uus) {
                        otsimiseMeetod((String) vana, (String) uus);
                    }
                }
        );
        otsingSisend.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                otsingList.refresh();
            }
        });

        // Nupud
        Button lisNupp = new Button("Lisa");
        lisNupp.setMinSize(70, 10);
        lisNupp.setOnAction(new LisamiseKuulamine());
        Button kusNupp = new Button("Kustuta");
        kusNupp.setOnAction(e -> {
            ObservableList<Raamat> valitudAndmed, k6ikAndmed;
            k6ikAndmed = tabel.getItems();
            valitudAndmed = tabel.getSelectionModel().getSelectedItems();
            valitudAndmed.forEach(k6ikAndmed::remove);
        });
        kusNupp.setMinSize(70, 10);
        Button salNupp = new Button("Salvesta");
        salNupp.setOnAction(e -> {
            try {
                andmeteFailiKirjutamine();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        salNupp.setMinSize(70, 10);
        Button suvNupp = new Button("Vajuta siia");
        suvNupp.setMinSize(100, 10);

        // HBox-d ja VBox-d
        HBox tiitelHb = new HBox(100);
        tiitelHb.getChildren().add(tiitel);
        HBox nupudHb = new HBox(5);
        nupudHb.getChildren().addAll(pealkiriSisend, autorSisend, aastaSisend, kogusSisend, lisNupp, kusNupp, salNupp);
        VBox otsingVb = new VBox(10);
        otsingVb.getChildren().addAll(otsingSisend, otsingList);
        VBox statsVb = new VBox(15);
        statsVb.getChildren().addAll(stats, raamatudErinevad, raamatudKokku);
        HBox statsOtsHb = new HBox(300);
        statsOtsHb.getChildren().addAll(otsingVb, statsVb);
        VBox suurVb = new VBox(10);
        suurVb.setPadding(new Insets(15, 15, 15, 15));
        suurVb.getChildren().addAll(tiitelHb, tabel, nupudHb, otsing, statsOtsHb);

        // Kujundus
        Scene stseen = new Scene(suurVb, 820, 650);
        stseen.getStylesheets().add("Puhas.css");
        peaAken.setScene(stseen);
        peaAken.show();
    }

    // ================================================================================================================
    //                                                 M E E T O D I D
    // ================================================================================================================

    /**
     * Meetod loeb faili "arhiiv.csv" rida-rida haaval ning yhes reas on kogu informatsioon
     * yhe Raamatu parameetrite kohta. Paremeetrite eraldajaks on faili tekstis on symbol "/".
     * Igast reast moodustatakse Raamatu parameetreid sisaldav massiiv. Iga massiivi abil luuakse
     * uus Raamat objekt. Kõik Raamat-d lisatakse tyhja ObservableList-i.
     *
     * @return nimekirja "andmed", mis sisaldab Raamat objekte
     */
    private ObservableList<Raamat> tabeliT2itmine() {
        List<Raamat> nimekiri = new ArrayList<>();
        File arhiiv = new File("arhiiv.csv");
        BufferedReader br = null;
        String rida = "";
        String eraldaja = "/";
        try {
            if (!arhiiv.exists()) {
                arhiiv.createNewFile();
            }
            br = new BufferedReader(new FileReader(arhiiv));
            while ((rida = br.readLine()) != null) {
                String[] massiiv = rida.split(eraldaja);
                nimekiri.add(
                        new Raamat(massiiv[0].trim(), massiiv[1].trim(), Integer.parseInt(massiiv[2]), Integer.parseInt(massiiv[3])));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        ObservableList<Raamat> andmed = FXCollections.observableArrayList(nimekiri);
        return andmed;
    }

    /**
     * Meetodis kysitakse Raamat klassist objekti Raamat parameetrite v22rtused ning
     * kirjutakse need faili "arhiiv.csv". Parameetreid iseloomustavaid kirjeid
     * eraldadakse symboliga "/".
     *
     * @throws Exception failiga yhenduse saamise rikke korral
     */
    public void andmeteFailiKirjutamine() throws Exception {
        Writer kirj = null;
        File arhiiv = new File("arhiiv.csv");
        try {
            if (!arhiiv.exists()) {
                arhiiv.createNewFile();
            }
            File fail = new File("arhiiv.csv");
            kirj = new BufferedWriter(new FileWriter(fail));
            for (Raamat raamat : andmed) {
                String text = raamat.getPealkiri().trim() + "/" + raamat.getAutor().trim() + "/" + raamat.getAasta() + "/"
                        + raamat.getKogus() + "\n";
                kirj.write(text);
            }
        } catch (Exception ex) {
            System.out.println("Tühjad väljad");
            ex.printStackTrace();
        } finally {
            kirj.flush();
            kirj.close();
        }
    }


    /**
     * Meetod loev faili "arhiiv.csv" read ja paigutab need tyhja ObservableList-i. Ridadest
     * eemaldadakse eraldussymbolid "/".
     *
     * @return nimekirja faili "arhiiv.csv" ridadega
     */
    public ObservableList<String> tooresNimekiri() {
        File arhiiv = new File("arhiiv.csv");
        ObservableList<String> nimekiri = FXCollections.observableArrayList();
        try {
            BufferedReader br = new BufferedReader(new FileReader(arhiiv));
            String rida = "";
            while ((rida = br.readLine()) != null) {
                String ridaPuhas = rida.replace('/', ' ');
                nimekiri.add(ridaPuhas);
            }
            return nimekiri;
        } catch (IOException e) {
            System.out.println("Fail puudub.");
        }
        return null;
    }

    /**
     * Meetod, mille abil kuvataks faili ridade nimekirja esitamise vaatesse need faili
     * read, mis sisaldavad otisngulahtrisse sisestud symboleid. Kasutakse ListView-na yldmuutujana
     * defineeritud otsingList-i. Nimekiri muutub juhul, kui otsingu sisestus lahtrisse sisestaakse
     * uus symbol. Symbolid grupeeritakse vastavalt nendevaheliste tyhikute j2rgi ning symbolite
     * grupid paigutakse massiivi. Kontollidakse, kas iga symboligrupi yhtivust iga s6nega
     * otsingListi igal real. Moodustakse uus otsingList, mis sisaldab ridasid, mis omakorda
     * sisaldavad sisestuslahtris olevaid symbolite gruppe.
     * <p>
     * Kasutatud materjali
     * http://www.drdobbs.com/jvm/simple-searching-in-java/232700121 (10.12.2015)
     *
     * @param vana on kirje sisestuslahtris enne uue symboli sisestamist
     * @param uus  on kirje sisestuslahtris sel hetkel, kui on lisatud uus symbol
     */
    public void otsimiseMeetod(String vana, String uus) {
        if (vana != null && uus.length() < vana.length()) {
            otsingList.setItems(tooresNimekiri());
        }
        String[] katked = uus.toUpperCase().split(" ");
        ObservableList<String> poolikudSisestused = FXCollections.observableArrayList();
        for (Object sisestus : otsingList.getItems()) {
            boolean sobivus = true;
            String sisendTekst = (String) sisestus;
            for (String i : katked) {
                if (!sisendTekst.toUpperCase().contains(i)) {
                    sobivus = false;
                    break;
                }
            }
            if (sobivus) {
                poolikudSisestused.add(sisendTekst);
            }
            otsingList.setItems(poolikudSisestused);
        }
    }

    /**
     * Meetodis loetakse failist "arhiiv.csv" kõik objektide Raamat ainult sama paremeetrit
     * iseloomustavad kirjed. Kasutatav ainult String-tyypi parameetrite puhul.
     *
     * @param tulbaNr tabeli tulbanumber, mis sisaldav objekt Raamatu vastavat String-tyypi
     *                parameerit.
     * @return nimekiri objektide Raamat yhtedest paremeetritest
     */
    public static ObservableList<String> tulbaList(int tulbaNr) {
        File arhiiv = new File("arhiiv.csv");
        ObservableList<String> list = FXCollections.observableArrayList();
        String rida = "";
        String eraldaja = "/";
        try {
            BufferedReader br = new BufferedReader(new FileReader(arhiiv));
            while ((rida = br.readLine()) != null) {
                String[] ajutine = rida.split(eraldaja);
                list.add(ajutine[tulbaNr]);
            }
            return list;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Meetodis loetakse failist "arhiiv.csv" kõik objektide Raamat ainult sama paremeetrit
     * iseloomustavad kirjed. Kasutatav ainult Integer-tyypi parameetrite puhul. Leitakse
     * k6ikide Integer-tyypi parameetrite summa.
     *
     * @param tulbaNr tabeli tulbanumber, mis sisaldav objekt Raamatu vastavat Integer-tyypi
     *                parameerit.
     * @return objekti vastava Integer-tyypi parameetrite summa
     */
    public static int listiNumbriteSumma(int tulbaNr) {
        int sum = 0;
        ObservableList<Integer> intList = FXCollections.observableArrayList();
        for (String s : tulbaList(tulbaNr)) {
            intList.add(Integer.valueOf(s));
        }
        sum = intList.stream().mapToInt(Integer::intValue).sum();
        return sum;
    }

    // ================================================================================================================
    //                                          N U P P U D E    K L A S S I D
    // ================================================================================================================

    /**
     * Klass, mis rakendab klassi EventHandler v6imalusi. Seda klassi kasutakse tabelisse
     * uue objekti lisamisel. Klass sisaldab meetodit, kus on paika pandud kriteeriumid,
     * selle kohta, milliseid kirjeid sisetuslahtrid aksepteerivad.
     */
    private class LisamiseKuulamine implements EventHandler<ActionEvent> {
        /**
         * Meetod, mis k2ivitav aktsiooni.
         *
         * @param e on aktsiooni iseloomustav parameeter
         */
        @Override
        public void handle(ActionEvent e) {
            int aastaT2na = Calendar.getInstance().get(Calendar.YEAR);
            if (pealkiriSisend.getText().trim().equals("")) {
                Hypikaknad.sisestamiseViga("Sisesta raamatu pealkiri.");
            } else {
                try {
                    int aastaInt = Integer.parseInt(aastaSisend.getText());
                    int kogusInt = Integer.parseInt(kogusSisend.getText());
                    if (aastaT2na < aastaInt || aastaInt <= 1800 && aastaInt != 0) {
                        aastaSisend.clear();
                        Hypikaknad.sisestamiseViga("Aasta peab olema arv vahemikus 1800 kuni " + aastaT2na);
                    } else {
                        andmed.add(new Raamat(pealkiriSisend.getText().trim(), autorSisend.getText().trim(), aastaInt, kogusInt));
                        pealkiriSisend.clear();
                        autorSisend.clear();
                        aastaSisend.clear();
                        kogusSisend.clear();
                    }
                } catch (Exception e2) {
                    aastaSisend.clear();
                    kogusSisend.clear();
                    Hypikaknad.sisestamiseViga("Aasta või kogus ei sisestatud arvuna. Sisesta aasta ja kogus uuesti.\n"
                            + "Kui raamatu ilmumisaasta pole teada, sisesta aasta väärtuseks 0.");
                }
            }
        }
    }
}
