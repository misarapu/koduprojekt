/**
 * Raamat
 * Loodud 10. detsember 2015
 *
 * @author Mikk Sarapuu <mikksarapuu@gmail.com>
 */

package main;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Klassis defineeritakse k6ik parameetrid, mis on vajalikud objekti Raamatu loomiseks.
 */
public class Raamat {
    /**
     * Objekti Raamatu parameetrid defineerime Property-dena, sest Raamatu-d paigutatakse
     * ObservableList-i ning Property kujul muutujate puhul on vajadusel nendega ObservableList-s
     * lihtsam manipuleerida.
     */
    public SimpleStringProperty pealkiri;
    public SimpleStringProperty autor;
    public SimpleIntegerProperty aasta;
    public SimpleIntegerProperty kogus;

    /**
     * Obejkti Raamatu konstruktor
     *
     * @param pk on raamatu pealkiri
     * @param au on raamatu autor
     * @param a  on raamatu ilmumisaasta
     * @param k  on yhe raamatu kogus
     */
    public Raamat(String pk, String au, int a, int k) {
        this.pealkiri = new SimpleStringProperty(pk);
        this.autor = new SimpleStringProperty(au);
        this.aasta = new SimpleIntegerProperty(a);
        this.kogus = new SimpleIntegerProperty(k);
    }

    /**
     * @return raamatu pealkirja
     */
    public String getPealkiri() {
        return pealkiri.get();
    }

    /**
     * @return raamatu autori
     */
    public String getAutor() {
        return autor.get();
    }

    /**
     * @return raamatu ilmumisaasta
     */
    public int getAasta() {
        return aasta.get();
    }

    /**
     * @return yhe raamatu koguse
     */
    public int getKogus() {
        return kogus.get();
    }
}
